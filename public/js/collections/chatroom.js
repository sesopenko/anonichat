/**
 * Created with IntelliJ IDEA.
 * User: sean
 * Date: 11/16/13
 * Time: 3:33 PM
 * (c) Sean Esopenko 2013
 */
define(['backbone', 'models/user'], function(Backbone, User){
    var ChatroomCollection = Backbone.Collection.extend({
        model:User
    });
    return ChatroomCollection;
});