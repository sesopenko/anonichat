/**
 * Created with IntelliJ IDEA.
 * User: sean
 * Date: 11/16/13
 * Time: 11:28 AM
 * (c) Sean Esopenko 2013
 */
define(['backbone', 'JSON', 'key/storage', 'models/identity'], function(Backbone, JSON, Storage, Identity){
    var WalletCollection = Backbone.Collection.extend({
        model:Identity,
        saveStorage:function(){
            var thisWallet = this;
            console.log('encoding:', thisWallet.models);
            var json = JSON.stringify(thisWallet.models);
            console.log('saving:', json);
            Storage.set('wallet', json);
        }
    });
    WalletCollection.loadStorage = function(app){
        var walletString = Storage.get('wallet');
        if(walletString && typeof walletString == 'string'){
            var walletJson = JSON.parse(walletString);
            if(!walletJson){
                walletJson = {};
            }
        }
        console.log('setting wallet:', walletJson);
        var wallet = new WalletCollection(walletJson);
//        wallet.app = app;
        return wallet;
    };
    return WalletCollection;
});