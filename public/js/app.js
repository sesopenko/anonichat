/**
 * Created with IntelliJ IDEA.
 * User: sean
 * Date: 11/11/13
 * Time: 3:20 PM
 * (c) Sean Esopenko 2013
 */
define([
    'jquery','underscore','backbone','router','io', 'JSON',
    'key/privatekey', 'com/chat', 'key/storage',
    'collections/wallet'
], function($, _, Backbone, Router, io, JSON,
            PrivateKey, Chat, Storage,
            WalletCollection
    ){
    var App = Backbone.Model.extend({
        initialize:function(){
            this.set('privateKey',new PrivateKey());
            this.get('privateKey').loadStorage();
            this.set('chat', new Chat());
            this.get('chat').connect();
            this.set('password', Storage.get('password'));
            this.set('wallet', WalletCollection.loadStorage(this));
            console.log('set wallet:', this.get('wallet'));
            this.set('router', new Router(this));
            this.get('router').start();
        },
        defaults:{
            privateKey:null,
            password:null
        },
        setPassword:function(password){
            var thisApp = this;
            if(thisApp.validPassword(password)){
                thisApp.set('password', password);
                Storage.set('password', password);
                return true;
            } else {
                return false;
            }
        },
        validPassword:function(password){
            if(typeof password != 'string'){
                console.log('not string');
                return false;
            }
            if(password.length < 8){
                console.log('not 8 characters');
                return false;
            }
            if(!password.match(/^[ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789]+$/)){
                console.log('not alphanumeric');
                return false;
            }
            return true;
        }
    });
    return App;

});