if(!Window.console){Window.console = {};}
if(!Window.console.log){Window.console.log = function(){};}
String.prototype.trim = function(){
    return this.replace(/^\s+|\s+$/g, "");
};
String.prototype.htmlEntities = function(){
    return this.toString().replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/'/g, '&#039;').replace(/"/g,'&#034;');
};

require.config({
    paths:{
        jquery:'vendor/jquery/jquery',
        underscore:'vendor/underscore/underscore',
        backbone:'vendor/backbone/backbone',
        'jquery-ui':'vendor/jquery/jquery-ui',
        JSON:'vendor/json/json2',
        io:'/socket.io/socket.io',
        cryptico:'vendor/cryptico/cryptico',
        CryptoJS:'vendor/cryptojs/compiled',
        'md5':'vendor/md5/md5'
    },
    shim:{
        'CryptoJS':{
            exports:'CryptoJS'
        },
        'md5':{
            exports:'md5'
        },
        'cryptico':{
            exports:'cryptico'
        },
        'socket.io':{
            exports:'io'
        },
        'JSON':{
            exports:'JSON'
        },
        'jquery-ui':{
            exports:'$',
            deps:['jquery']
        },
        'backbone':{
            deps:['underscore','jquery'],
            exports:'Backbone'
        },
        'underscore':{
            exports:'_'
        },
        'bootstrap':['jquery']
    }
});

require([
    'app'
], function(App){
    var app = new App();
})