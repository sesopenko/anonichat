/**
 * Created with IntelliJ IDEA.
 * User: sean
 * Date: 11/11/13
 * Time: 3:51 PM
 * (c) Sean Esopenko 2013
 */
define(['underscore','jquery','backbone'
], function(_, $, Backbone){
    var IndexView = Backbone.View.extend({
        el:$('#container'),
        render:function(){
            var data = {};
            var indexTemplate = $('#indexView').html();
            var compiledTemplate = _.template(indexTemplate, data);
            this.$el.empty().append(compiledTemplate);
        }
    });
    return IndexView;
});