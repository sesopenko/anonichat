/**
 * Created with IntelliJ IDEA.
 * User: sean
 * Date: 11/11/13
 * Time: 5:25 PM
 * (c) Sean Esopenko 2013
 */
define(['underscore','jquery-ui','backbone','key/privatekey', 'helpers'
], function(_, $, Backbone, PrivateKey, Helpers){
    var SetupKeysView = Backbone.View.extend({
        initialize:function(app){
            this.app = app;
        },
        el:$('#container'),
        privateKey:null,
        render:function(){
            var setupTemplate = $('#setupView').html();
//            var data = {privateKey:this.privateKey}
            window.app = this.app;
            var data = {
                privateKey:this.app.get('privateKey'),
                password:this.app.get('password')
            };
            var compiledTemplate = _.template(setupTemplate, data);
            this.$el.empty().append(compiledTemplate);
            this.interactivity();
        },
        interactivity:function(){
            var thisView = this;
            var passInteractive = function(){
                $('input[name=changePass]', thisView.$el).click(function(){
                    var $changePass = $(this);
                    var $p = $(this).parent();
                    var html = "<input name='newPassword' type='password' value='' placeholder='Enter password here...'/> " +
                        "<input name='showPassword' type='checkbox'> Show Password</input> " +
                        "<input name='randomPassword' type='button' value='Randomize'/> " +
                        "<input name='setPassword' type='button' value='Set Password'/> " +
                        "<input name='cancelPassword' type='button' value='Cancel'/>";
                    $p.empty().append(html);
                    var $pass = $('input[name=newPassword]', $p);
                    $('input[name=showPassword]', $p).change(function(){
                        if($(this).is(':checked')){
                            $pass.attr('type', 'text');
                        } else {
                            $pass.attr('type', 'password');
                        }
                    });
                    $('input[name=randomPassword]', $p).click(function(){
                        var random = PrivateKey.randomString(16);
                        $pass.val(random);
                    });
                    $('input[name=setPassword]', $p).click(function(){
                        var password = $pass.val();
                        thisView.setPassword(password);
                    });
                    $('input[name=cancelPassword]', $p).click(function(){
                        $p.empty().append($changePass);
                        passInteractive();
                    });
                });
            };
            passInteractive();
            $('input[name=generateKey]', thisView.$el).click(function(){
                var confirm = function(confirmed){
                    if(confirmed){
                        var privateKey =  thisView.app.get('privateKey');
                        privateKey.generate();
                        thisView.render();

                    }
                };
                Helpers.confirm("Regenerate Key?", "<div><ul><li>" +
                    "Generating a new <i>private key</i> is like generating a new identity." +
                    "</li>" +
                    "<li>Your previous <i>public key</i> will no longer be valid for those you chat with.</li>" +
                    "<li>Unless you've made a backup of your previous <i>private key</i>, this cannot be undone.</li></ul></div>" +
                    "<div>Are you sure you want to generate a new <i>private key</i>?</div>", confirm, true);
            });
            $('input[name=backupKey]', thisView.$el).click(function(){
                var dataUri = "<a href='data:application/octet-stream;charset=utf-8,"+thisView.app.get('privateKey').passPhrase.htmlEntities()+"' target='blank'>Download Backup</a>";
                Helpers.simpleMessage("Download Private Key Backup", dataUri, true);
            })

        },
        setPassword:function(password){
            thisView = this;
            if(thisView.app.validPassword(password)){
                console.log('valid password');
                var confirmFunction = function(confirmed){
                    if(confirmed){
                        thisView.app.setPassword(password);
                        thisView.render();
                    }
                }
                Helpers.confirm("Set Password?", "<div><ul><li>" +
                    "If there is a previously set password it will be lost.</li>" +
                    "<li>You will have to share this new password with all people before they can " +
                    "communicate with you. </li></div>" +
                    "</ul>" +
                    "<div>Are you sure you want to reset it?</div>", confirmFunction, true);
            } else {
                Helpers.errorMessage("Invalid Password", "Your password just be alphanumeric and minimum 8 characters.");
            }
        }
    });
    return SetupKeysView;
});