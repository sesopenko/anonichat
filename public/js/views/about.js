/**
 * Created with IntelliJ IDEA.
 * User: sean
 * Date: 11/11/13
 * Time: 3:51 PM
 * (c) Sean Esopenko 2013
 */
define(['underscore','jquery','backbone'
], function(_, $,Backbone){
    var AboutView = Backbone.View.extend({
        el:$('#container'),
        render:function(){
            var data = {};
            var aboutTemplate = $('#aboutView').html();
            var compiledTemplate = _.template(aboutTemplate, data);
            this.$el.empty().append(compiledTemplate);
        }
    });
    return AboutView;
});