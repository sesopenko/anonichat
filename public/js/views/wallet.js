/**
 * Created with IntelliJ IDEA.
 * User: sean
 * Date: 11/16/13
 * Time: 11:15 AM
 * (c) Sean Esopenko 2013
 */
define(['underscore','jquery','backbone', 'helpers', 'models/identity'
], function(_, $,Backbone, Helpers, Identity){
    var WalletView = Backbone.View.extend({
        initialize:function(app){
            var thisView = this;
            this.app = app;
            var wallet = this.app.get('wallet');
            wallet.bind('add',function(){
                thisView.render();
            });
            wallet.bind('remove', function(){
                thisView.render();
            });
            wallet.on('change:password', function(){
                console.log('change fired');
                thisView.render();
            });
        },
        el:$('#container'),
        render:function(){
            var wallet = this.app.get('wallet');
            var data = {wallet:wallet};
            var walletTemplate = $('#walletView').html();
            var compiledTemplate = _.template(walletTemplate, data);
            this.$el.empty().append(compiledTemplate);
            this.interactivity();
        },
        interactivity:function(){
            var thisView = this;
            var wallet = thisView.app.get('wallet');
            var $name = $('input[name=name]', thisView.$el);
            var $publicKey = $('textarea[name=publicKey]', thisView.$el);
            var $password = $('input[name=password]', thisView.$el);
            var $showPassword = $('input[name=showPassword]', thisView.$el);
            var $addIdentity = $('input[name=addIdentity]', thisView.$el);

            $showPassword.change(function(){
                var inputType = $(this).is(':checked') ? 'text' : 'password';
                $password.attr('type', inputType);
            });
            $addIdentity.click(function(){
                var id = {
                    name:$name.val().trim(),
                    publicKey:$publicKey.val().trim(),
                    password:$password.val()
                };
                console.log('validating:', id);
                var identityErrors = Identity.validate(id);
                if(identityErrors){
                    Helpers.errorMessage("Invalid", identityErrors);
                } else {
                    console.log('adding:', id);
//                    var identity = new Identity(id);
                    wallet.add([id]);
                    wallet.saveStorage();
                    console.log('new wallet:', wallet);

                }
            });

            //identity actions
            $('input[name=changePass]', thisView.$el).click(function(){
                var cid = $(this).attr('data-cid');
                var d = "<div title='New Password'>" +
                    "New Password: " +
                    "<input name='password' type='password' placeholder='Person\'s password/><br>" +
                    "<input name='showPassword' type='checkbox'>Show Password</input>" +
                    "</div>"
                var $d = $(d);
                $('input[name=showPassword]', $d).change(function(){
                    var inputType = $(this).is(':checked') ? 'text' : 'password';
                    $('input[name=password]', $d).attr('type', inputType);
                });
                $d.dialog({
                    modal:true,
                    width:400,
                    autoOpen:true,
                    close:function(){
                        $(this).remove();
                    },
                    buttons:{
                        'Change Password':function(){
                            var password = $('input[name=password]', $d).val();
                            if(!Identity.validPass(password)){
                                Helpers.errorMessage("Invalid Password", "Password cannot be less than 8 characters");
                                return;
                            }
                            var identity = wallet.get(cid);
                            identity.set('password', password);
                            console.log('new identity:', identity);
                            wallet.saveStorage();
                            $(this).dialog('close');

                        },
                        'Cancel':function(){
                            $(this).dialog('close');
                        }
                    }
                })
            });
            $('input[name=delete]', thisView.$el).click(function(){
                var cid = $(this).attr('data-cid');
                var confirm = function(confirmed){
                    if(confirmed){
                        var identity = wallet.get(cid);
                        console.log('destroying:', identity);
                        identity.destroy();
                        wallet.remove(identity);
                        wallet.saveStorage();
                        console.log('removed.  wallet:', wallet);

                    }
                };
                Helpers.confirm("Delete Identity?", "Are you sure you want to delete this identity?  This cannot be undone", confirm);

            });
        }
    });
    return WalletView;
});