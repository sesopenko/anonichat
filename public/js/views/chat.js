/**
 * Created with IntelliJ IDEA.
 * User: sean
 * Date: 11/11/13
 * Time: 4:01 PM
 * (c) Sean Esopenko 2013
 */
define(['underscore','jquery-ui','backbone','CryptoJS','com/chat'
], function(_, $,Backbone,CryptoJS,Chat){
    var EnterKey = 13;
    var ChatView = Backbone.View.extend({
        initialize:function(app){
            console.log('chat.app:', app);
            this.app = app;
            this.chat = app.get('chat');
        },
        el:$('#container'),
        render:function(){
            this.renderJoin();
        },
        renderJoin:function(){
            var data = {};
            var chatTemplate = $('#chatView').html();
            var compiledTemplate = _.template(chatTemplate, data);
            this.$el.empty().append(compiledTemplate);
            this.roomInteractivity();
            console.log('rendered room picker');
        },
        roomInteractivity:function(){
            var thisChat = this;
            var $roomName = $('input[name=roomName]', thisChat.$el);
            var joinRoom = function(){
                var app = thisChat.app;
                var privateKey = app.get('privateKey');
                var publicKey = privateKey.publicKey;
                var roomName = $roomName.val().trim();
                console.log('joining room:', roomName);
                thisChat.chat.joinRoom(roomName, publicKey);
                console.log('rendering room:', roomName);
                thisChat.renderRoom(roomName);
                console.log('rendered room');
            };
            $roomName.keyup(function(event){
                if(event.which == EnterKey){
                    joinRoom();
                }
            });
            $('input[name=joinRoom]', thisChat.$el).click(function(){
                joinRoom();
            });
        },
        renderRoom:function(roomName){
            var thisView = this;

            console.log('rendering chat for ', roomName);
            var data = {roomName:roomName};
            var chatTemplate = $('#chatRoom').html();
            var compiledTemplate = _.template(chatTemplate, data);
            this.$el.empty().append(compiledTemplate);
            this.chatInteractivity();
            console.log('rendered chat');
        },
        sendMessage:function(){
            var thisView = this;
            var app = thisView.app;
            var privateKey = thisView.app.get('privateKey');
            var publicKey = privateKey.publicKey;
            var password = app.get('password');
            var $sendMessage = $('input[name=message]', this.$el);
            var message = $sendMessage.val();
            var encrypted = CryptoJS.AES.encrypt(message, password);
            var cipher = {
                publicKey:publicKey,
                ciphertext:encrypted.ciphertext,
                salt:encrypted.salt
            };
            if(this.chat){
                this.chat.sendMessage(cipher);
                $sendMessage.val('');
            } else {
                console.log('not connected');
            }

        },
        chatInteractivity:function(){
            var thisView = this;
            $('input[name=message]', this.$el).keyup(function(event, ui){
                if(event.which == EnterKey){
                    thisView.sendMessage();
                }
            });
            $('input[name=sendMessage]', this.$el).click(function(){
                thisView.sendMessage();
            });
            var messageCallback = function(cipher){
                var app = thisView.app;
                var wallet = app.get('wallet');
                if(cipher.publicKey && typeof cipher.publicKey == 'string'){
                    var identity = wallet.findWhere({publicKey:cipher.publicKey});
                    if(!identity){
                        thisView.lockedMessage();
                        console.log("no identity for ", cipher.publicKey);
                        return;
                    }
                    console.log('got message from:', identity);
                    var password = identity.get('password');
                    var name = identity.get('name');
                    var decrypted = CryptoJS.AES.decrypt(cipher, password);
                    if(!decrypted){
                        return;
                    }
                    var decryptedString = decrypted.toString(CryptoJS.enc.Utf8);
                    if(!decryptedString){
                        thisView.lockedMessage();
                        return;
                    }
                    var safeMessage = decryptedString.htmlEntities();
                    var $message = $("<div class='message'>" +
                        "<span class='name'>"+name.htmlEntities()+":</span> " +
                        "<span class='message'>"+safeMessage+"</span>" +
                        "</div>");
                    $('div#chatLog').append($message);
                } else {
                    return;
                }
            };
            this.chat.onReceive(messageCallback);
            this.chat.room.on('reset', function(){
                thisView.renderRoomList();
            });
        },
        lockedMessage:function(){
            var locked = "<div class='message locked'><span class='ui-icon ui-icon-locked'></span><span>Unknown User</span></div>"
            $('div#chatLog').append(locked);
        },
        close:function(){
//            this.remove();
            this.unbind();
            if(this.chat.connected){
                this.chat.disconnect();
            }
        },
        renderRoomList:function(){
            var thisView = this;
            var app = thisView.app;
            var wallet = app.get('wallet');
            var chat = thisView.chat;
            var room = chat.room;
            var template = $('#roomMembers').html();
            var data = {room:room,wallet:wallet,ownKey:app.get('privateKey').publicKey};
            var compiledTemplate = _.template(template, data);
            $('div#members', thisView.$el).empty().append(compiledTemplate);

//            this.$el.empty().append(compiledTemplate);

        }
    });
    return ChatView;
});