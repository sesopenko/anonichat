/**
 * Created with IntelliJ IDEA.
 * User: sean
 * Date: 11/11/13
 * Time: 3:21 PM
 * (c) Sean Esopenko 2013
 */
define([
    'jquery','underscore','backbone',
    'views/about','views/index','views/chat','views/setupkeys', 'views/wallet'
], function($,_,Backbone,
    AboutView, IndexView, ChatView, SetupKeysView, WalletView){
    var AppRouter = Backbone.Router.extend({
        initialize:function(app){
            var thisRouter = this;
            thisRouter.app = app;

            thisRouter.on('route:showIndex', function(actions){
                var indexView = new IndexView();
                indexView.render();
            });
            thisRouter.on('route:showSetupKeys', function(actions){
                var keysView = new SetupKeysView(app);
                keysView.render();
            });
            thisRouter.on('route:showAbout', function(actions){
                var aboutView = new AboutView();
                aboutView.render();
            });
            thisRouter.on('route:showChat', function(actions){
                var chatView = new ChatView(thisRouter.app);
                chatView.render();
            });
            thisRouter.on('route:defaultAction',function(actions){
                console.log('404 view');
            });
            thisRouter.on('route:showWallet', function(actions){
                var walletView = new WalletView(thisRouter.app);
                walletView.render();
            })
        },
        routes:{
            '':'showIndex',
            'setupkeys':'showSetupKeys',
            'about':'showAbout',
            'chat':'showChat',
            'wallet':'showWallet',
            '*actions':'defaultAction'
        },
        start:function(){
            Backbone.history.start();
        }
    });
    return AppRouter;

    var initialize = function(app){
        console.log('router.initialize:', app);
        var appRouter = new AppRouter;
        var currentView = null;
        appRouter.on('route:showIndex', function(actions){
            if(currentView && currentView.close){
                currentView.close();
            }
            var indexView = new IndexView();
            currentView = indexView;
            indexView.render();
        });
        appRouter.on('route:showSetupKeys', function(actions){
            if(currentView && currentView.close){
                currentView.close();
            }
            var keysView = new SetupKeysView(app);
            currentView = keysView;
            keysView.render();
        });
        appRouter.on('route:showAbout', function(actions){
            if(currentView && currentView.close){
                currentView.close();
            }
            var aboutView = new AboutView();
            currentView = aboutView;
            aboutView.render();
        });
        appRouter.on('route:showChat', function(actions){
            if(currentView && currentView.close){
                curentView.close();
            }
            var chatView = new ChatView();
            currentView = chatView;
            chatView.render();
        });
        appRouter.on('route:defaultAction',function(actions){
            if(currentView && currentView.close){
                currentview.close();
            }
            currentView = null;
            console.log('404 view');
        });
        Backbone.history.start();
        console.log('router initialized');
        return appRouter;
    };
    return {
        initialize:initialize
    };
});