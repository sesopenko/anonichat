/**
 * Created with IntelliJ IDEA.
 * User: sean
 * Date: 11/16/13
 * Time: 11:20 AM
 * (c) Sean Esopenko 2013
 */
define(['backbone'], function(Backbone){
    var Identity = Backbone.Model.extend({
        defaults:{
            'name':'No Name',
            'publicKey':'',
            'password':''
        },
        checkSignature:function(message){

        },
        decrypt:function(message){

        }
    });
    Identity.validPass = function(password){
        return password.length >= 8;
    }
    Identity.validate = function(attrs){
        var errors = [];
        if(attrs.name == ''){
            errors.push("Name cannot be blank");
        }
        if(attrs.publicKey == ''){
            errors.push("Public Key cannot be blank");
        }
        if(!Identity.validPass(attrs.password)){
            errors.push("Password cannot be less than 8 characters");
        }
        return errors.length > 0 ? errors : false;
    };

    return Identity;
});