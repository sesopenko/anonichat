/**
 * Created with IntelliJ IDEA.
 * User: sean
 * Date: 11/16/13
 * Time: 3:34 PM
 * (c) Sean Esopenko 2013
 */
define(['backbone'], function(Backbone){
    var User = Backbone.Model.extend({
        defaults:{
            'socketId':null,
            'publicKey':null
        }
    });
    return User;
});