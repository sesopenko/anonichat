/**
 * Created with IntelliJ IDEA.
 * User: sean
 * Date: 11/12/13
 * Time: 10:43 PM
 * (c) Sean Esopenko 2013
 */
define([], function(){
    var supported = {
        fileApi:window.File && window.FileReader && window.fileList && window.Blob,
        webSockets:Modernizr.webSockets,
        localStorage:Modernizr.localStorage
    };
    return supported;
});