/**
 * Created with IntelliJ IDEA.
 * User: sean
 * Date: 11/11/13
 * Time: 5:29 PM
 * (c) Sean Esopenko 2013
 */
define([], function(){
    var Storage = {
        supported:function(){
            try{
                return 'localStorage' in window && window['localStorage'] !== null;
            } catch (e){
                return false;
            }
        },
        set:function(itemName, itemValue){
            if(Storage.supported()){
                localStorage.setItem(itemName, itemValue);
            }
        },
        get:function(itemName){
            if(Storage.supported()){
                return localStorage.getItem(itemName);
            } else {
                return null;
            }
        }
    };
    return Storage;
});