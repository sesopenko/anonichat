/**
 * Created with IntelliJ IDEA.
 * User: sean
 * Date: 11/11/13
 * Time: 5:35 PM
 * (c) Sean Esopenko 2013
 *
 * Uses:  https://github.com/wwwtyro/cryptico
 */
define(['cryptico', 'md5', 'key/storage', ], function(cryptico, md5, Storage){
    var PrivateKey = function(){
        var thisKey = this;
        thisKey.privateKey = null;
        thisKey.publicKey = null;
        thisKey.passPhrase = null;
        thisKey.publicHash = null;
        thisKey.bits = 512;
        thisKey.password = null;
        thisKey.loadStorage = function(){
            var genPhrase = Storage.get('genPhrase');
            thisKey.generate(genPhrase);
        };
        thisKey.saveStorage = function(){
            Storage.set('genPhrase', this.passPhrase);
        };
        thisKey.generate = function(genPhrase){
            var passPhrase = genPhrase ? genPhrase : PrivateKey.randomString();
            thisKey.passPhrase = passPhrase;
            console.log('passPhrase:', passPhrase);
            var bits = thisKey.bits;
            thisKey.privateKey = cryptico.generateRSAKey(passPhrase, bits);
            console.log('privateKey:', thisKey.privateKey);
            thisKey.publicKey = cryptico.publicKeyString(thisKey.privateKey);
            console.log('publicKey:', thisKey.publicKey);
            thisKey.publicHash = md5(thisKey.publicKey);
            console.log('publicHash:', thisKey.publicHash);
            thisKey.saveStorage();
        }
    };
    PrivateKey.randomString = function(target_length){
        var length = target_length || 128;
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        var text = "";
        for(var i=0; i < length; i++){
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        }
        return text;
    }
    return PrivateKey;
});