/**
 * Created with IntelliJ IDEA.
 * User: sean
 * Date: 11/11/13
 * Time: 3:39 PM
 * (c) Sean Esopenko 2013
 */
define(['io', 'models/user', 'collections/chatroom'], function(io, User, ChatroomCollection){

    var Chat = function(){
        var thisChat = this;
        thisChat.messages = [];
        thisChat.port = '80';
        thisChat.connected = false;
        thisChat.room = new ChatroomCollection([]);
        thisChat.connect = function(){
            console.log('connecting to web socket');
            thisChat.socket = io.connect('http://localhost:'+thisChat.port);
            console.log('connected to web socket');
            thisChat.connected = true;
            thisChat.socket.on('message', function(data){
                if(data.message){
                    thisChat.messages.push(data.message);
                    console.log('got message:', data.message);
                    if(thisChat.receiveCallback){
                        console.log('calling callback function');
                        thisChat.receiveCallback(data.message);
                    } else {
                        console.log('no callback function');
                    }
                } else {
                    console.log('There is a proboem:', data);
                }
            });
            thisChat.socket.on('room', function(data){
                console.log('room list:', data);
                if(typeof data == 'object'){
                    var users = [];
                    for(socketId in data){
                        var user = data[socketId];
                        if(user.publicKey && typeof user.publicKey == 'string' && user.socketId && typeof user.socketId == 'string'){
                            users.push(user);
                        }
                    }
                    thisChat.room.reset(users);
                }
            });
            console.log('finished initializing chat');
        };
        thisChat.onReceive = function(receive_callback){
            thisChat.receiveCallback = receive_callback;
        };
        thisChat.sendMessage = function(message){
            thisChat.socket.emit('send', {message:message});
        };
        thisChat.disconnect = function(){
            if(thisChat.socket){
                console.log('disconnecting from socket');
                thisChat.socket.disconnect();
            }
        };
        thisChat.joinRoom = function(roomName, publicKey){
            console.log('joining room: ', roomName);
            thisChat.socket.emit('joinRoom', {roomName:roomName, publicKey:publicKey});
        };

    }
    return Chat;
})