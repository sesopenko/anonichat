/**
 * Created with IntelliJ IDEA.
 * User: sean
 * Date: 11/12/13
 * Time: 9:55 PM
 * (c) Sean Esopenko 2013
 */
define(['jquery-ui','underscore'], function($, _){
    var Helpers = {
        errorMessage:function(title, message, callback){
            var messageHtml = message;
            if(typeof messageHtml == 'object'){
                messageHtml = "<ul>";
                _.each(message, function(error){
                    messageHtml += "<li>"+error.htmlEntities()+"</li>";
                });
                messageHtml += "</ul>";
            } else {
                messageHtml = messageHtml.htmlEntities();
            }
            var html = "<div class='errorMessage' title='"+title.htmlEntities()+"'>"+
                messageHtml+
                "</div>";
            var $d = $(html).dialog({
                autoOpen:true,
                modal:true,
                width:400,
                close:function(){
                    if(callback){
                        callback();
                    }
                    $(this).remove();
                },
                buttons:{
                    'OK':function(){
                        $(this).dialog('close');
                    }
                }
            });
        },
        confirm:function(title, message, callback, noEscape){
            var html = "<div class='confirmMessage' title='"+title.htmlEntities()+"'>";
            if(noEscape){
                html += message;
            } else {
                html += message.htmlEntities();
            }
            html += "</div>";
            var $d = $(html).dialog({
                autoOpen:true,
                modal:true,
                width:400,
                close:function(){
                    $(this).remove();
                },
                buttons:{
                    'Yes':function(){
                        if(callback){
                            callback(true);
                        }
                        $(this).dialog('close');
                    },
                    'No':function(){
                        if(callback){
                            callback(false);
                        }
                        $(this).dialog('close');
                    }
                }
            });
        },
        simpleMessage:function(title, message, noEscape){
            var html = "<div class='confirmMessage' title='"+title.htmlEntities()+"'>";
            if(noEscape){
                html += message;
            } else {
                html += message.htmlEntities();
            }
            html += "</div>";
            var $d = $(html).dialog({
                autoOpen:true,
                modal:true,
                width:400,
                close:function(){
                    $(this).remove();
                },
                buttons:{
                    'OK':function(){
                        $(this).dialog('close');
                    }
                }
            });
        }
    };
    return Helpers;
});