
/**
 * Module dependencies.
 */

var express = require('express');
var routes = require('./routes');
var user = require('./routes/user');
var http = require('http');
var path = require('path');

var app = express();

// all environments
app.set('port', process.env.PORT || 80);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));


// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}
var server = http.createServer(app);
server.listen(app.get('port'));
var io = require('socket.io').listen(server);
//app.get('/', routes.index);
app.get('/users', user.list);

//http.createServer(app).listen(app.get('port'), function(){
//  console.log('Express server listening on port ' + app.get('port'));
//});
//var io = require('socket.io').listen(http.createServer(app).listen(app.get('port')));

console.log('Express & socket.io server listening on port ' + app.get('port'));
//var io = require('socket.io').listen(app.listen(app.get('port')));

var sockets = {};
var rooms = {};

io.sockets.on('connection', function(socket){
//    console.log(socket);
    var currentRoom = null;
    socket.on('send', function(data){
        console.log('socket.id:', socket.id);
        if(currentRoom){
//            io.sockets.emit('message', data);
            data.message.socketId = socket.id;
            console.log(data);
            socket.broadcast.to(currentRoom).emit('message', data);
        }
    });
    socket.on('joinRoom', function(data){
        console.log('joinRoom:', data);
        if(data.roomName && typeof data.roomName == 'string' && data.publicKey && typeof data.publicKey == 'string'){
            if(currentRoom){
                socket.leave(currentRoom);
            }
            currentRoom = data.roomName;
            console.log('joined', data.roomName);
            socket.join(data.roomName);
            console.log('sending joinRoom to everybody else');
            socket.broadcast.to(data.roomName).emit('joinRoom', {publicKey:data.publicKey,socketId:socket.id});
            sockets[socket.id] = data.roomName;
            var thisRoom = rooms[data.roomName];
            var user = {socketId:socket.id,publicKey:data.publicKey};
            if(!thisRoom){
                //create the room
                rooms[data.roomName] = {};
            }
            //add to the room
            rooms[data.roomName][socket.id] = user;
            //update the clients so they know who's in the room
            io.sockets.in(data.roomName).emit('room', rooms[data.roomName]);
        }
    });
    socket.on('disconnect', function(){
        if(sockets[socket.id]){
            if(currentRoom){
                //remove them from the room
                var userRoom = rooms[currentRoom];
                delete userRoom[socket.id];
                io.sockets.in(currentRoom).emit('room', rooms[currentRoom]);
            }
            //finally remove them from the socket list
            delete sockets[socket.id]
        }
    });
});
